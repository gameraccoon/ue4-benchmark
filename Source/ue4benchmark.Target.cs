// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class ue4benchmarkTarget : TargetRules
{
	public ue4benchmarkTarget(TargetInfo Target) : base(Target)
    {
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "ue4benchmark" } );
	}
}
