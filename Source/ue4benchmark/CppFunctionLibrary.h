// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "CppFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class UE4BENCHMARK_API UCppFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static int CalculateSomethingCpp(int InNumTimes);
	
};
