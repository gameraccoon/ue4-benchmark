// Fill out your copyright notice in the Description page of Project Settings.

#include "ue4benchmark.h"
#include "CppFunctionLibrary.h"

int UCppFunctionLibrary::CalculateSomethingCpp(int InNumTimes)
{
	TArray<int> TestArray;

	const int ELEMENTS_NUM = 100;
	const int MAX_RAND = 10000;
	const int NUM_TIMES = InNumTimes;
	int MinSum = 0; // to prevent redundant optimizations
	int Min = 0; // here because BPs have no inner visibility scopes

	// same as BP (this way choosen because BPs have no Reserve() function)
	for (int i = 0; i < ELEMENTS_NUM; ++i)
	{
		TestArray.Push(i);
	}

	for (int i = 0; i < NUM_TIMES; ++i)
	{
		for (int j = 0; j < ELEMENTS_NUM; ++j)
		{
			TestArray[j] = FMath::RandHelper(MAX_RAND); // same as BP
		}

		Min = MAX_RAND;
		for (auto& Number : TestArray)
		{
			if (Number < Min)
			{
				Min = Number;
			}
		}
		MinSum += Min;
	}

	return MinSum;
}
