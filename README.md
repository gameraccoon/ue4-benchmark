Related to this topic:
https://forums.unrealengine.com/community/general-discussion/118970-c-and-blueprints-speed-comparison


### Last results with UE5.1


Average results of a few runs on Windows Shipping version (IterationsCount = 100000):

BP ~4652ms

C++ ~173ms

Blueprint version of the function is ~27 times slower than C++ version.
